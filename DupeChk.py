#! /usr/bin/python3

import os, sys, hashlib
from collections import defaultdict

# creates a hash value for each file in the given directory tree
# returns a tuple that of (hash, path)
def checksum(targ):
    md5=hashlib.md5()
    with open(targ, "rb") as t:
        for i in iter(lambda: t.read(1024), b""):
            md5.update(i)
    return md5.hexdigest(), targ
# loops through the given directory tree and creates a list of dictionaries:
# [hash : (path to file with that hash, path to file with that hash, etc.)
def hashing():
    fs, ih, haLS, pLS=[], [], [], []
    fDict=defaultdict(list)
# rounds up files and their paths recursively throughout the given directory tree
# and makes a list of all paths to all files
    for start, direc, files in os.walk(sys.argv[1]):
        for fls in files:
            fs.append(os.path.join(start, fls))
# loops the hashing of each file in the tree and adds those tuples to a list (hash, path)
    for i in fs:
        ih.append(checksum(i))
       # print("\n", ih, "\n")
# loops through all hash and path pairs, making a dictionary for each hash
# and appends all path with matching hashes into a list as the value for that hash
    for i in ih[0:]:
        fDict.setdefault(i[0], []).append(i[1])
    return fDict
#def prompting(comLS):
    
def main():
    ihLS=hashing()
# TESTING BELOW_________________________________________
    for i in ihLS.items():
        print ("\n")
        print (i)
    print("\n\n")
    print(len(ihLS))
# TESTING ABOVE_________________________________________
if __name__=="__main__":
    main()


